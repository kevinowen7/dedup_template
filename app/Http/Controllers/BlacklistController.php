<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Blacklist;
use App\Models\BlacklistAlias;
use App\Models\BlacklistBulk;
use App\Models\BlacklistError;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Common\Type;
use App\Http\Controllers\CookieController;
use Session;
use Response;

class BlacklistController extends Controller {

    public function get()
    {
        return Blacklist::with("alias")->orderBy('id', 'DESC')->paginate(20);
    }

    public function getById(Request $req)
    {
        return Blacklist::with("alias")->where("id",$req->id)->first();
    }

    public function getByBulkId(Request $req)
    {
        return Blacklist::with("alias")->where("id_bulk",$req->id)->orderBy('id', 'DESC')->paginate(20);
    }

    public function store(Request $req)
    {
        try {
            $id_user = Users::where("nik",Session::get('nik'))->first();
            if ($id_user === null) {
                $id_user = null;
            } else {
                $id_user = $id_user["id"];
            }
            
            $blacklist = Blacklist::create([
                'id_user' => $id_user,
                'id_bulk' => $req->id_bulk,
                'name' => $req->name,
                'nik' => $req->nik,
                'address' => $req->address,
                'birth_date' => $req->birth_date,
                'data_mentah' => $req->data_mentah
            ]);
            $id_blacklist = $blacklist->id;
            $alias_name = $req->alias_name;

            $blacklist = BlacklistAlias::create([
                'id_blacklist' => $id_blacklist,
                'alias_name' => $req->name,
                'nik' => $req->nik,
                'address' => $req->address,
                'birth_date' => $req->birth_date
            ]);
    
            foreach ($alias_name as $alias){
                $blacklist = BlacklistAlias::create([
                    'id_blacklist' => $id_blacklist,
                    'alias_name' => $alias,
                    'nik' => $req->nik,
                    'address' => $req->address,
                    'birth_date' => $req->birth_date
                ]);
            }
    
            return $blacklist;

        } catch (\Exception $e) {
            return 0;
        }
    }

    public function update(Request $req)
    {
        $blacklist = Blacklist::where("id",$req->id)->update([
            'id_bulk' => $req->id_bulk,
            'name' => $req->name,
            'nik' => $req->nik,
            'address' => $req->address,
            'birth_date' => $req->birth_date,
            'data_mentah' => $req->data_mentah
        ]);

        return $blacklist;
    }

    public function destroy(Request $req)
    {
        $blacklist = Blacklist::where("id",$req->id)->delete();
        return $blacklist;
    }


    //blacklist Alias crud
    public function update_alias(Request $req)
    {   
        try {
            BlacklistAlias::where("id_blacklist",$req->id_blacklist)->delete();

            foreach($req->name as $name){
                $blacklist = BlacklistAlias::create([
                    'id_blacklist' => $req->id_blacklist,
                    'alias_name' => $name,
                    'nik' => $req->nik,
                    'address' => $req->address,
                    'birth_date' => $req->birth_date
                ]);
            }

            return Response::json(["status" => 1, "data" => null]);
        } catch (\Exception $e) {
            return Response::json(["status" => 0, "data" => $e->getMessage()]);
        }
    }

    public function destroy_alias(Request $req)
    {
        $blacklist = BlacklistAlias::where("id",$req->id)->delete();
        return $blacklist;
    }




    //upload bulk
    public function getBulk()
    {
        return BlacklistBulk::with(["blacklist","blacklist_error","users" => function($q){
            $q->with("branch");
        }])->orderBy('id', 'DESC')->paginate(20);
    }

    public function bulkImport(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xls,xlsx'
        ]);

        $fileName = time()."_".request()->file->getClientOriginalName();

        $directory = 'public/uploads';
        if (!Storage::has($directory)) {
           Storage::makeDirectory($directory);
        }

        try {
            request()->file->move(storage_path('app/public/uploads'), $fileName);
            $reader = ReaderEntityFactory::createXLSXReader();
            $reader->open(storage_path('app/public/uploads/'. $fileName)); //open the file   

            $id_user = Users::where("nik",Session::get('nik'))->first();
            if ($id_user === null) {
                $id_user = null;
            } else {
                $id_user = $id_user["id"];
            }
            

            foreach ($reader->getSheetIterator() as $sheet) {
                $i = 0;
                $data_gagal = 0;
                
                $bulk_input = BlacklistBulk::create([
                    'id_user' => $id_user,
                    'file_name' => $fileName
                ]);

                $dump_bl = Blacklist::where("id_bulk","!=",null)->get();
                foreach ($dump_bl as $bl){
                    BlacklistAlias::where("id_blacklist",$bl->id)->delete();
                }
                Blacklist::where("id_bulk","!=",null)->delete();
                BlacklistError::truncate();

                foreach ($sheet->getRowIterator() as $row) {
                    // skip header excel
                    if ($i==0){
                        $i=1;
                        continue;
                    } else{
                        try {
                            $rows = $row->toArray();

                            $nama = $rows[0];
                            $nama  = explode('alias',strtolower($nama));

                            $nik = $rows[1];
                            $nik  = explode(' ',strtolower($nik));
                            $nik_final = null;
                            foreach ($nik as $n){
                                $n = preg_replace('/[^A-Za-z0-9\-]/', '', $n);
                                if (is_numeric($n) && strlen($n)>6){
                                    $nik_final = $n;
                                    break;
                                }
                            }

                            $birth_date = $rows[5];
                            $f_date = date('Y-m-d',strtotime($birth_date));
                            
                            $address = $rows[7];
                            $data_mentah = $rows;
                            
                            //if nik exist
                            $exist_blacklist = Blacklist::where("nik",$nik_final)->get();
                            if ($nik_final!=null && count($exist_blacklist)>0){
                                foreach ($nama as $alias){
                                    $blacklist = BlacklistAlias::create([
                                        'id_blacklist' => $exist_blacklist[0]->id,
                                        'alias_name' => $alias,
                                        'nik' => $nik_final,
                                        'address' => $address,
                                        'birth_date' => $f_date,
                                        'data_mentah' => json_encode($data_mentah)
                                    ]);
                                }
                            } else {
                                $blacklist_data = Blacklist::create([
                                    'id_user' => $id_user,
                                    'id_bulk' => $bulk_input->id,
                                    'name' => $nama[0],
                                    'nik' => $nik_final,
                                    'address' => $address,
                                    'birth_date' => $f_date,
                                    'data_mentah' => json_encode($data_mentah)
                                ]);

                                foreach ($nama as $alias){
                                    $blacklist = BlacklistAlias::create([
                                        'id_blacklist' => $blacklist_data->id,
                                        'alias_name' => $alias,
                                        'nik' => $nik_final,
                                        'address' => $address,
                                        'birth_date' => $f_date,
                                        'data_mentah' => json_encode($data_mentah)
                                    ]);
                                }
                            }
                        } catch (\Exception $e) {
                            $blacklist_data = BlacklistError::create([
                                'id_user' => $id_user,
                                'id_bulk' => $bulk_input->id,
                                'data_mentah' => json_encode($rows)
                            ]);
                            $data_gagal++;
                        }

                        //remove to get all data
                        //break;
                    }
                    $i++;
                }

                BlacklistBulk::where("id",$bulk_input->id)->update([
                    'total_data' => $i,
                    'total_gagal' => $data_gagal,
                ]);
                break;
            }

            //return
            return Response::json([
                "status" => 1,
                "data" => [
                    'total_data' => $i,
                    'total_gagal' => $data_gagal
                ]
            ]);

        } catch (\Exception $e) {
            error_log($e);
            //delete file in public directory
            if(File::exists('uploads/'.$fileName)) {
                File::delete('uploads/'.$fileName);
            }

            //delete file in public directory
            if(File::exists('uploads/results'.$fileName)) {
                File::delete('uploads/results'.$fileName);
            }

            Response::json([
                "status" => 0,
                "data" => null
            ]);
        }
    }

}