<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Branch;
use Cookie;

class BranchController extends Controller {

    public function index()
    {
        return Branch::orderBy('id', 'DESC')->get();
    }

}