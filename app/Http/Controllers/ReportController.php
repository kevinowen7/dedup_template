<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Report;
use Cookie;
use PDF;
use Spatie\PdfToImage\Pdf as pdfimg;
use Org_Heigl\Ghostscript\Ghostscript;
use Response;

class ReportController extends Controller {
    #example http://localhost:8000/api/report?page=1&admin_nik=201800000000&start_date=6/22/2000&end_date=6/25/2021&branch=1&status=1
    public function get(Request $req)
    {
        $report = Report::select("report.*")->with(["report_by"=> function($q){
            $q->with("branch");
        },"blacklist_data","data_client"])
        ->join("users","users.id","report.id_user")
        ->join("branch","branch.id","users.id_branch")
        ->orderBy('report.id', 'DESC');

        if ($req["admin_name"]!=null){
            $report = $report->where("users.name",$req["admin_name"]);
        }
        if ($req["admin_nik"]!=null){
            $report = $report->orWhere("users.nik",$req["admin_nik"]);
        }
        if ($req["start_date"]!=null && $req["end_date"]!=null){
            $from = date("Y-m-d H:i:s", strtotime($req["start_date"]));
            $to = date("Y-m-d H:i:s", strtotime($req["end_date"]));
            $report = $report->whereBetween("report.created_at",[$from, $to]);
        }
        if ($req["branch"]!=null){
            $report = $report->where("branch.id",$req["branch"]);
        }
        if ($req["status"]!=null){
            $report = $report->where("report.status",$req["status"]);
        }
        $report = $report->paginate(20);

        return $report;
    }

    public function getById(Request $req)
    {
        return Report::with(["report_by" => function($q){
            $q->with("branch");
        },"blacklist_data","data_client"])->where("id",$req->id)->first();
    }

    public function approve_report(Request $req)
    {
        return Report::where("id",$req->id)->update([
            'status' => 1
        ]);
    }

    public function reject_report(Request $req)
    {
        return Report::where("id",$req->id)->update([
            'status' => -1
        ]);
    }

    public function downloadPdf(Request $req)
    {

        $report = Report::select("report.*")->with(["report_by"=> function($q){
            $q->with("branch");
        },"blacklist_data","data_client"])
        ->join("users","users.id","report.id_user")
        ->join("branch","branch.id","users.id_branch")
        ->orderBy('report.id', 'DESC')
        ->where("report.id",$req->id)
        ->first();
        

        $pdf = PDF::setOptions( ['isHtml5ParserEnabled' , true] )->loadView('export_pdf',['data' => $report]);

        $directory = 'public/pdf';
        if (!Storage::has($directory)) {
           Storage::makeDirectory($directory);
        }

        $filename = 'pdf_'.date('Y-m-d-H-i-s').'.pdf';
        $path_file = storage_path('app/public/pdf/'.$filename);
        $pdf->save($path_file);
        return $pdf->download($filename);
    }

    public function downloadImg(Request $req)
    {

        $report = Report::select("report.*")->with(["report_by"=> function($q){
            $q->with("branch");
        },"blacklist_data","data_client"])
        ->join("users","users.id","report.id_user")
        ->join("branch","branch.id","users.id_branch")
        ->orderBy('report.id', 'DESC')
        ->where("report.id",$req->id)
        ->first();
        

        $pdf = PDF::setOptions( ['isHtml5ParserEnabled' , true] )->loadView('export_pdf',['data' => $report]);

        $directory = 'public/pdf';
        if (!Storage::has($directory)) {
           Storage::makeDirectory($directory);
        }

        $directory = 'public/img';
        if (!Storage::has($directory)) {
           Storage::makeDirectory($directory);
        }

        $filename = 'pdf_'.date('Y-m-d-H-i-s').'.pdf';
        $path_file = storage_path('app/public/pdf/'.$filename);
        $pdf->save($path_file);

        /*
        $gs_path = 'C:\Program Files\gs\gs9.54.0\bin\gswin64c.exe';
        Ghostscript::setGsPath($gs_path);
        */

        $img = new pdfimg($path_file);
        $filename_img = 'img_'.date('Y-m-d-H-i-s').'.png';
        $path_file_img = storage_path('app/public/img/'.$filename_img);
        $img->setOutputFormat("png")->saveImage($path_file_img);

        $headers = array('Content-Type: image/jpeg',);
        return Response::download($path_file_img, $filename_img,$headers);
    }

}