<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Checking;
use App\Models\Users;
use App\Models\BlacklistHistory;
use App\Models\Report;
use App\Models\BlacklistAlias;
use App\Models\CheckingBulk;
use App\Models\CheckingError;
use App\Models\DataClient;
use App\Models\HistoryDataClient;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Common\Type;
use App\Http\Controllers\CookieController;
use Session;
use Response;

class CheckingController extends Controller {

    #example = http://localhost:8000/api/checking?page=1&admin_nik=201800000000&start_date=6/22/2000&end_date=6/25/2021&branch=1
    public function get(Request $req)
    {
        $id_user = Users::where("nik",Session::get('nik'))->first();
        if ($id_user === null) {
            $id_user = null;
        } else {
            $id_user = $id_user["id"];
        }

        $checking = Checking::select("check_history.*")->with(['blacklist_data','admin' => function($q){
            $q->with("branch");
        },'data_client','report' => function($q){
            $q->with("report_by", function($q){
                $q->with("branch");
            });
        }])->join("users","users.id","check_history.id_user")
        ->where("check_history.id_user",$id_user)
        ->join("branch","branch.id","users.id_branch")
        ->orderBy('check_history.id', 'DESC');
        
        if ($req["admin_name"]!=null){
            $checking = $checking->where("users.name",$req["admin_name"]);
        }
        if ($req["admin_nik"]!=null){
            $checking = $checking->orWhere("users.nik",$req["admin_nik"]);
        }
        if ($req["start_date"]!=null && $req["end_date"]!=null){
            $from = date("Y-m-d H:i:s", strtotime($req["start_date"]));
            $to = date("Y-m-d H:i:s", strtotime($req["end_date"]));
            $checking = $checking->whereBetween("check_history.created_at",[$from, $to]);
        }
        if ($req["branch"]!=null){
            $checking = $checking->where("branch.id",$req["branch"]);
        }

        $checking = $checking->paginate(20);
        return $checking;
    }

    public function getById(Request $req)
    {
        return Checking::with(['blacklist_data','admin' => function($q){
            $q->with("branch");
        },'data_client','report' => function($q){
            $q->with("report_by", function($q){
                $q->with("branch");
            });
        }])->where("id",$req->id)->first();
    }

    public function getByBulkId(Request $req)
    {
        return Checking::with(['blacklist_data','admin','data_client','report' => function($q){
            $q->with("report_by", function($q){
                $q->with("branch");
            });
        }])->where("id_bulk",$req->id)->orderBy('id', 'DESC')->paginate(20);
    }

    public function store(Request $req)
    {
        try {
            $id_user = Users::where("nik",Session::get('nik'))->first();
            if ($id_user === null) {
                $id_user = null;
            } else {
                $id_user = $id_user["id"];
            }
            
            $checking = Checking::create([
                'id_user' => $id_user,
                'name' => $req->name,
                'nik' => $req->nik,
                'birth_date' => date("Y-m-d H:i:s", strtotime($req->birth_date))
            ]);
            $id_checking = $checking->id;

            $blacklist = BlacklistAlias::where(function ($query) use ($req){
                $query->whereRaw('LOWER(`alias_name`) LIKE ? ',['%'.strtolower($req->name).'%']);
                /*
                $query->whereRaw('LOWER(`alias_name`) LIKE ? ',['%'.strtolower($req->name).'%'])->where(function ($query) use ($req){
                    $query->where('birth_date', date("Y-m-d", strtotime($req->birth_date)));
                    $query->orWhere('birth_date',"");
                    $query->orWhere('birth_date',null);
                });
                */
            })->orWhere("nik",$req->nik)->get();
            
            foreach ($blacklist as $data){
                BlacklistHistory::create([
                    'id_history' => $id_checking,
                    'name' => $data->alias_name,
                    'nik' => $data->nik,
                    'birth_date' => $data->birth_date,
                    'data_mentah' => $data->data_mentah
                ]);
            }

            $checking = DataClient::where(function ($query) use ($req){
                //$query->whereRaw('LOWER(`name`) LIKE ? ',['%'.strtolower($req->name).'%']);
                $query->whereRaw('LOWER(`name`) LIKE ? ',['%'.strtolower($req->name).'%'])->where('DateOfBirth', date("Y-m-d H:i:s", strtotime($req->birth_date)));
            })->orWhere("KTP",$req->nik)->get();

            foreach ($checking as $data){
                HistoryDataClient::create([
                    'id_history' => $id_checking,
                    'name' => $data->name,
                    'DateOfBirth' => $data->DateOfBirth,
                    'OpenedDate' => $data->OpenedDate,
                    'KTP' => $data->KTP,
                    'ClientID' => $data->ClientID,
                    'RegionID' => $data->RegionID,
                    'CenterID' => $data->CenterID,
                    'CenterName' => $data->CenterName,
                    'accountid' => $data->accountid,
                    'OurBranchID' => $data->OurBranchID,
                    'status' => $data->status,
                    'wo_reason' => $data->wo_reason,
                    'FirstDisbursementDate' => $data->FirstDisbursementDate,
                    'LoanStatusID' => $data->LoanStatusID,
                    'GroupID' => $data->GroupID,
                    'GroupName' => $data->GroupName
                ]);
            }
            

            return Checking::with(['blacklist_data','admin'=> function($q){
                $q->with("branch");
            },'data_client','report' => function($q){
                $q->with("report_by", function($q){
                    $q->with("branch");
                });
            }])->where("id",$id_checking)->first();

        } catch (\Exception $e) {
            error_log($e);
            return 0;
        }
    }

    public function report(Request $req)
    {
        $id_user = Users::where("nik",Session::get('nik'))->first();
        if ($id_user === null) {
            $id_user = null;
        } else {
            $id_user = $id_user["id"];
        }

        return Report::create([
            'id_history' => $req->id_history,
            'id_user' => $id_user,
            'report_name' => $req->name,
            'report_nik' => $req->nik,
            'report_date' => $req->dob,
            'status' => 0
        ]);
    }


    public function getBulk()
    {
        return CheckingBulk::with(["users"=> function($q){
            $q->with("branch");
        },"check_error"])->orderBy('id', 'DESC')->paginate(20);
    }

    public function bulkImport(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xls,xlsx'
        ]);

        $fileName = time()."_".request()->file->getClientOriginalName();

        $directory = 'public/checking';
        if (!Storage::has($directory)) {
           Storage::makeDirectory($directory);
        }

        try {
            request()->file->move(storage_path('app/public/checking'), $fileName);
            $reader = ReaderEntityFactory::createXLSXReader();
            $reader->open(storage_path('app/public/checking/'. $fileName)); //open the file   

            $id_user = Users::where("nik",Session::get('nik'))->first();
            if ($id_user === null) {
                $id_user = null;
            } else {
                $id_user = $id_user["id"];
            }
            

            foreach ($reader->getSheetIterator() as $sheet) {
                $i = 0;
                $data_gagal = 0;
                
                $bulk_input = CheckingBulk::create([
                    'id_user' => $id_user,
                    'file_name' => $fileName
                ]);

                foreach ($sheet->getRowIterator() as $row) {
                    // skip header excel
                    if ($i==0){
                        $i=1;
                        continue;
                    } else{
                        try {
                            $rows = $row->toArray();

                            $nama = $rows[0];
                            $nik = $rows[1];
                            $birth_date = $rows[2];
                            
                            $checking = Checking::create([
                                'id_user' => $id_user,
                                'id_bulk' => $bulk_input->id,
                                'name' => $nama,
                                'nik' => $nik,
                                'birth_date' => $birth_date
                            ]);

                            $id_checking = $checking->id;

                            $blacklist = BlacklistAlias::where(function ($query) use ($nama,$birth_date){
                                $query->whereRaw('LOWER(`alias_name`) LIKE ? ',['%'.strtolower($nama).'%']);
                                /*
                                $query->whereRaw('LOWER(`alias_name`) LIKE ? ',['%'.strtolower($nama).'%'])->where(function ($query) use ($nama,$birth_date){
                                    $query->where('birth_date', $birth_date);
                                    $query->orWhere('birth_date', "");
                                    $query->orWhere('birth_date', null);
                                });
                                */
                            })->orWhere("nik",$nik)->get();
                            
                            foreach ($blacklist as $data){
                                BlacklistHistory::create([
                                    'id_history' => $id_checking,
                                    'name' => $data->alias_name,
                                    'nik' => $data->nik,
                                    'birth_date' => $data->birth_date,
                                    'data_mentah' => $data->data_mentah
                                ]);
                            }

                            
                            $checking = DataClient::where(function ($query) use ($nama,$birth_date){
                                //$query->whereRaw('LOWER(`name`) LIKE ? ',['%'.strtolower($nama).'%']);
                                $query->whereRaw('LOWER(`name`) LIKE ? ',['%'.strtolower($nama).'%'])->where('DateOfBirth', $birth_date);
                            })->orWhere("KTP",$nik)->get();
                            
                            foreach ($checking as $data){
                                HistoryDataClient::create([
                                    'id_history' => $id_checking,
                                    'name' => $data->name,
                                    'DateOfBirth' => $data->DateOfBirth,
                                    'OpenedDate' => $data->OpenedDate,
                                    'KTP' => $data->KTP,
                                    'ClientID' => $data->ClientID,
                                    'RegionID' => $data->RegionID,
                                    'CenterID' => $data->CenterID,
                                    'CenterName' => $data->CenterName,
                                    'accountid' => $data->accountid,
                                    'OurBranchID' => $data->OurBranchID,
                                    'status' => $data->status,
                                    'FirstDisbursementDate' => $data->FirstDisbursementDate,
                                    'LoanStatusID' => $data->LoanStatusID,
                                    'GroupID' => $data->GroupID,
                                    'GroupName' => $data->GroupName,
                                    'wo_reason' => $data->wo_reason
                                ]);
                            }

                        } catch (\Exception $e) {
                            $checking_data = CheckingError::create([
                                'id_user' => $id_user,
                                'id_bulk' => $bulk_input->id,
                                'data_mentah' => json_encode($rows)
                            ]);
                            $data_gagal++;
                        }

                        //remove to get all data
                        //break;
                    }
                    $i++;
                }

                CheckingBulk::where("id",$bulk_input->id)->update([
                    'total_data' => $i,
                    'total_gagal' => $data_gagal,
                ]);
                break;
            }

            //return
            return Response::json([
                "status" => 1,
                "data" => [
                    'total_data' => $i,
                    'total_gagal' => $data_gagal
                ]
            ]);

        } catch (\Exception $e) {
            error_log($e);
            //delete file in public directory
            if(File::exists(storage_path('app/public/checking'.$fileName))) {
                File::delete(storage_path('app/public/checking'.$fileName));
            }

            Response::json([
                "status" => 0,
                "data" => null
            ]);
        }
    }

    public function getDataClient()
    {
        return DataClient::orderBy('id', 'DESC')->paginate(20);
    }

    public function getDownload(){
        $file = public_path()."/import_single.xlsx";
        $headers = array('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',);
        return Response::download($file, 'import_single.xlsx',$headers);
    }

    public function exportHistory(Request $req){
        $id_user = Users::where("nik",Session::get('nik'))->first();
        if ($id_user === null) {
            $id_user = null;
        } else {
            $id_user = $id_user["id"];
        }

        $checking = Checking::select("check_history.*")->with(['blacklist_data','admin' => function($q){
            $q->with("branch");
        },'data_client','report' => function($q){
            $q->with("report_by", function($q){
                $q->with("branch");
            });
        }])->join("users","users.id","check_history.id_user")
        ->where("check_history.id_user",$id_user)
        ->join("branch","branch.id","users.id_branch")
        ->orderBy('check_history.id', 'DESC');
        
        if ($req["admin_name"]!=null){
            $checking = $checking->where("users.name",$req["admin_name"]);
        }
        if ($req["admin_nik"]!=null){
            $checking = $checking->orWhere("users.nik",$req["admin_nik"]);
        }
        if ($req["start_date"]!=null && $req["end_date"]!=null){
            $from = date("Y-m-d H:i:s", strtotime($req["start_date"]));
            $to = date("Y-m-d H:i:s", strtotime($req["end_date"]));
            $checking = $checking->whereBetween("check_history.created_at",[$from, $to]);
        }
        if ($req["branch"]!=null){
            $checking = $checking->where("branch.id",$req["branch"]);
        }

        $checking = $checking->get();

        $directory = 'public/export';
        if (!Storage::has($directory)) {
           Storage::makeDirectory($directory);
        }

        $writer = WriterEntityFactory::createXLSXWriter();
        $filename = 'export_'.date('Y-m-d-H-i-s').'.xlsx';
        $path_file = storage_path('app/public/export/'.$filename);
        $writer->openToFile($path_file);
        // header
        $cells = [
            WriterEntityFactory::createCell("Name"),
            WriterEntityFactory::createCell("NIK"),
            WriterEntityFactory::createCell("Birth Date"),
            WriterEntityFactory::createCell("Admin NIK"),
            WriterEntityFactory::createCell("Admin Name"),
            WriterEntityFactory::createCell("Branch ID"),
            WriterEntityFactory::createCell("Checking Date"),
            WriterEntityFactory::createCell("Data Client"),
            WriterEntityFactory::createCell("Data Blacklist"),
            WriterEntityFactory::createCell("Data Report")
        ];

        $row = WriterEntityFactory::createRow($cells);
        $writer->addRow($row);
        //end header
        //data
        foreach ($checking as $data){
            $client_name = "";
            foreach ($data->data_client as $cl){
                $client_name = $client_name . "Nama : ".$cl->name."\n";
                $client_name = $client_name . "DateOfBirth : ".$cl->DateOfBirth."\n";
                $client_name = $client_name . "KTP : ".$cl->KTP."\n";
                $client_name = $client_name . "\n";
            }

            $blacklist_name = "";
            foreach ($data->blacklist_data as $bl){
                $blacklist_name = $blacklist_name . "Nama : ".$bl->name."\n";
                $blacklist_name = $blacklist_name . "DateOfBirth : ".$bl->birth_date."\n";
                $blacklist_name = $blacklist_name . "KTP : ".$bl->nik."\n";
                $blacklist_name = $blacklist_name . "\n";
            }

            $report_name = "";
            foreach ($data->report as $rp){
                $report_name = $report_name . "Report Name : ".$rp->report_name."\n";
                $report_name = $report_name . "Report NIK : ".$rp->report_nik."\n";
                $report_name = $report_name . "Report By : ".$rp->report_by->name." (".$rp->report_by->nik.") "."\n";
                $report_name = $report_name . "\n";
            }

            $cells = [
                WriterEntityFactory::createCell($data->name),
                WriterEntityFactory::createCell($data->nik),
                WriterEntityFactory::createCell($data->birth_date),
                WriterEntityFactory::createCell($data->admin->nik),
                WriterEntityFactory::createCell($data->admin->name),
                WriterEntityFactory::createCell($data->admin->branch->branchid),
                WriterEntityFactory::createCell($data->created_at->format('Y-m-d H:m:i')),
                WriterEntityFactory::createCell($client_name),
                WriterEntityFactory::createCell($blacklist_name),
                WriterEntityFactory::createCell($report_name),
                WriterEntityFactory::createCell("‏‏‎ ‎"),
            ];

            $row = WriterEntityFactory::createRow($cells);
            $writer->addRow($row);
        }
        $writer->close();

        $headers = array('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',);
        return Response::download($path_file, $filename,$headers);
    }

}