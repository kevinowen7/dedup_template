<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use Cookie;

class RoleController extends Controller {

    public function index()
    {
        return Role::get();
    }

}