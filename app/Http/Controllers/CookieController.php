<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Handler\CurlMultiHandler;
use GuzzleHttp\HandlerStack;
use App\Models\Users;
use Session;
use Response;

class CookieController extends Controller {

    public function getCookie()
    {
        return Session::get('user');
    }

    public function getNameCookie()
    {
        return Session::get('name');
    }

    public function getNikCookie()
    {
        return Session::get('nik');
    }

    /*
        @request
        {
            "user","pass"
        }
    */
    public function setCookie(Request $req)
    {
        $res = $this->login($req);

        if ($res!="0"){
            Session::put('user', $res["token"]);
            Session::put('user_id', $res["row"]["user_id"]);
            Session::put('name', $res["row"]["name"]);
            Session::put('nik', $req->user);

            return Response::json(["status" => 1, "data" => null]);
        } else {
            return Response::json(["status" => 0, "data" => null]);
        }
    }

    public function removeCookie()
    {
        Session::put('user',null);
        Session::put('user_id',null);
        Session::put('name',null);
        Session::put('nik',null);
        return redirect("/login");
    }

    public function logOut()
    {
        $this->removeCookie();
        return Response::json(["status" => 1, "data" => null]);
    }

    public function loginChecker()
    {
        if (Session::get('user') === null){
            return Response::json(["status" => 0, "data" => null]);
        } else {
            $res = $this->tokenChecker(Session::get('user'),Session::get('user_id'));
            if ($res=="1"){
                $user = Users::where("nik",Session::get('nik'))->with(["role","branch"])->first();
                return Response::json(["status" => 1, "data" => $user]);
            } else {
                return Response::json(["status" => 0, "data" => null]);
            }
        }
    }

    public function login(Request $req)
    {
        $data = [
            "nik" => $req->user,
            "password" => md5($req->pass)
        ];

        $headers = ['Content-Type' => 'application/json'];
        // call DB
        /*
        $users = Users::where("nik", $req->user)->where("password", md5($req->pass))->first();
        if ($users){
            return 1;
        } else {
            return "0";
        }
        */
        
        try{
            $client = new Client();
            $res = $client->request('POST', env('SSO_URL').'/api/User/loginUser',
                [
                    'headers' => $headers,
                    'json' => $data,
                    'verify' => false
                ]
            );

            $statuscode = $res->getStatusCode();
            $res = json_decode($res->getBody(),true);

            if (200 === $statuscode) {
                if ($res['status']==1){
                    // call DB
                    $users = Users::where("nik", $data['nik'])->where("password", $data['password'])->first();
                    if ($users){
                        return $res['data'];
                    } else {
                        return "0";
                    }
                } else {
                    return "0";
                }
            }
            else {
                return "0";
            }
        } catch (\Exception $e) {
            return "0";
        }
        
    }

    public function store(Request $req)
    {
        $data = [
            "nik" => $req->nik,
            "password" => md5($req->pass)
        ];

        $headers = ['Content-Type' => 'application/json'];

        try {
            //create on DB
            $user = Users::create([
                'name' => $req->name,
                'id_branch' => $req->id_branch,
                'id_role' => $req->id_role,
                'nik' => $req->nik,
                'is_active' => 1,
                'password' => md5($req->pass)
            ]);
            return Response::json(["status" => 1, "data" => $user]);
        } catch (\Exception $e) {
            return Response::json(["status" => 0, "data" => null]);
        }
        /*
        try{
            $client = new Client();
            $res = $client->request('POST', env('SSO_URL').'/api/User/loginUser',
                [
                    'headers' => $headers,
                    'json' => $data,
                    'verify' => false
                ]
            );

            $statuscode = $res->getStatusCode();
            $res = json_decode($res->getBody(),true);

            if (200 === $statuscode) {
                if ($res['status']==1){
                    //create on DB
                    Users::create([
                        'name' => $res['data']["row"]["name"],
                        'id_branch' => $req->id_branch,
                        'id_role' => $req->id_role,
                        'nik' => $req->user,
                        'is_active' => 1,
                        'password' => $data['password']
                    ]);

                    return Response::json(["status" => 1, "data" => $res['data']]);
                } else {
                    return Response::json(["status" => 0, "data" => null]);
                }
            }
            else {
                return Response::json(["status" => 0, "data" => null]);
            }
        } catch (\Exception $e) {
            return Response::json(["status" => 0, "data" => null]);
        }
        */
    }

    public function getUser()
    {
        return Users::with(["role","branch"])->get();
    }

    public function destroy(Request $req)
    {
        $users = Users::where("id",$req->id)->delete();
        return $users;
    }

    public function tokenChecker($token,$user_id)
    {
        $data = [
            "bavtoken" => $token,
            "userid" => $user_id
        ];

        $headers = ['Content-Type' => 'application/json'];

        try{
            $client = new Client();
            $res = $client->request('POST', env('SSO_URL').'/api/User/cekToken',
                [
                    'headers' => $headers,
                    'json' => $data,
                    'verify' => false
                ]
            );

            $statuscode = $res->getStatusCode();
            $res = json_decode($res->getBody(),true);

            if (200 === $statuscode) {
                if ($res['status']==1){
                    return "1";
                } else {
                    return "0";
                }
            }
            else {
                return "0";
            }
        } catch (\Exception $e) {
            return "0";
        }
    }

}