<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlacklistHistory;
use App\Models\Checking;
use App\Models\Report;
use Cookie;
use Carbon\Carbon;

class DashboardController extends Controller {

    public function get(Request $req)
    {
        $blacklist = BlacklistHistory::orderBy('id', 'DESC');
        $checking = Checking::orderBy('id', 'DESC');
        $report = Report::orderBy('id', 'DESC');

        $jumlah_not_include_bl = Checking::whereDoesntHave("blacklist_data");
        $jumlah_include_bl = Checking::whereHas("blacklist_data");

        $jumlah_not_include_dc = Checking::whereDoesntHave("data_client");
        $jumlah_include_dc = Checking::whereHas("data_client");

        $jumlah_include_all = Checking::whereHas("data_client")->whereHas("blacklist_data");
        $jumlah_not_include_all = Checking::whereDoesntHave("data_client")->whereDoesntHave("blacklist_data");

        if ($req["filter"]!=null){
            if ($req["filter"]=="daily"){
                $blacklist = $blacklist->whereDate("created_at",date("Y-m-d"));
                $checking = $checking->whereDate("created_at",date("Y-m-d"));
                $report = $report->whereDate("created_at",date("Y-m-d"));
                $jumlah_not_include_bl = $jumlah_not_include_bl->whereDate("created_at",date("Y-m-d"));
                $jumlah_include_bl = $jumlah_include_bl->whereDate("created_at",date("Y-m-d"));
                $jumlah_not_include_dc = $jumlah_not_include_bl->whereDate("created_at",date("Y-m-d"));
                $jumlah_include_dc = $jumlah_include_bl->whereDate("created_at",date("Y-m-d"));
                $jumlah_include_all = $jumlah_include_all->whereDate("created_at",date("Y-m-d"));
                $jumlah_not_include_all = $jumlah_not_include_all->whereDate("created_at",date("Y-m-d"));

            } else if ($req["filter"]=="montly"){
                $blacklist = $blacklist->where(
                    'created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString()
                );
                $checking = $checking->where(
                    'created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString()
                );
                $report = $report->where(
                    'created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString()
                );
                $jumlah_not_include_bl = $jumlah_not_include_bl->where(
                    'created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString()
                );
                $jumlah_include_bl = $jumlah_include_bl->where(
                    'created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString()
                );
                $jumlah_not_include_dc = $jumlah_not_include_bl->where(
                    'created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString()
                );
                $jumlah_include_dc = $jumlah_include_bl->where(
                    'created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString()
                );
                $jumlah_include_all = $jumlah_include_all->where(
                    'created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString()
                );
                $jumlah_not_include_all = $jumlah_not_include_all->where(
                    'created_at', '>=', Carbon::now()->firstOfMonth()->toDateTimeString()
                );
            } else if ($req["filter"]=="yearly"){
                $blacklist = $blacklist->whereYear('created_at', now()->year);
                $checking = $checking->whereYear('created_at', now()->year);
                $report = $report->whereYear('created_at', now()->year);
                $jumlah_not_include_bl = $jumlah_not_include_bl->whereYear('created_at', now()->year);
                $jumlah_include_bl = $jumlah_include_bl->whereYear('created_at', now()->year);
                $jumlah_not_include_dc = $jumlah_not_include_bl->whereYear('created_at', now()->year);
                $jumlah_include_dc = $jumlah_include_bl->whereYear('created_at', now()->year);
                $jumlah_include_all = $jumlah_include_all->whereYear('created_at', now()->year);
                $jumlah_not_include_all = $jumlah_not_include_all->whereYear('created_at', now()->year);
            }
        }
        $blacklist = $blacklist->count();
        $checking = $checking->count();
        $report = $report->count();

        $jumlah_not_include_bl = $jumlah_not_include_bl->count();
        $jumlah_include_bl = $jumlah_include_bl->count();

        $jumlah_not_include_dc = $jumlah_not_include_dc->count();
        $jumlah_include_dc = $jumlah_include_dc->count();

        $jumlah_include_all= $jumlah_include_all->count();
        $jumlah_not_include_all= $jumlah_not_include_all->count();

        $data = [
            "count_blacklist" => $blacklist,
            "count_checking" => $checking,
            "count_report" => $report,
            "blacklist_chart" => [
                "jumlah_include" => $jumlah_include_bl,
                "jumlah_not_include" => $jumlah_not_include_bl,
            ],
            "dedup_chart" => [
                "jumlah_include" => $jumlah_include_dc,
                "jumlah_not_include" => $jumlah_not_include_dc,
            ],
            "all_chart" => [
                "jumlah_include_all" => $jumlah_include_all,
                "jumlah_not_include_all" => $jumlah_not_include_all,
            ]
        ];
        return $data;
    }

}