<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BlacklistBulk extends Model
{
    protected $table = 'bulk_blacklist';
    protected $guarded = [];

    public function blacklist()
    {
        return $this->hasMany(Blacklist::class, 'id_bulk', 'id');
    }

    public function blacklist_error()
    {
        return $this->hasMany(BlacklistError::class, 'id_bulk', 'id');
    }
    
    public function users()
    {
        return $this->hasOne(Users::class, 'id', 'id_user');
    }
}
