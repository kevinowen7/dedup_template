<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class HistoryDataClient extends Model
{
    protected $table = 'history_data_client';
    protected $guarded = [];
}
