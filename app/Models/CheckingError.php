<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CheckingError extends Model
{
    protected $table = 'check_error';
    protected $guarded = [];
}
