<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class Report extends Model
{
    use Compoships;
    protected $table = 'report';
    protected $guarded = [];

    public function report_by()
    {
        return $this->hasOne(Users::class, 'id', 'id_user');
    }

    public function blacklist_data()
    {
        return $this->hasMany(BlacklistHistory::class, 'id_history', 'id_history');
    }

    public function data_client()
    {
        return $this->hasMany(HistoryDataClient::class, 'id_history', 'id_history');
    }
}
