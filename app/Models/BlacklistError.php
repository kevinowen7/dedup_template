<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BlacklistError extends Model
{
    protected $table = 'blacklist_error';
    protected $guarded = [];
}
