<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{
    protected $table = 'blacklist';
    protected $guarded = [];

    public function alias()
    {
        return $this->hasMany(BlacklistAlias::class, 'id_blacklist', 'id');
    }
}
