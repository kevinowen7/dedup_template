<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BlacklistAlias extends Model
{
    protected $table = 'alias_blacklist';
    protected $guarded = [];
}
