<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CheckingBulk extends Model
{
    protected $table = 'bulk_check';
    protected $guarded = [];
    
    public function users()
    {
        return $this->hasOne(Users::class, 'id', 'id_user');
    }

    public function check_error()
    {
        return $this->hasMany(CheckingError::class, 'id_bulk', 'id');
    }
}
