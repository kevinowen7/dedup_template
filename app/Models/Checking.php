<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class Checking extends Model
{
    use Compoships;
    protected $table = 'check_history';
    protected $guarded = [];

    public function blacklist_data()
    {
        return $this->hasMany(BlacklistHistory::class, 'id_history', 'id');
    }

    public function report()
    {
        return $this->hasMany(Report::class, ['report_nik','report_name'], ['nik','name']);
    }

    public function data_client()
    {
        return $this->hasMany(HistoryDataClient::class, 'id_history', 'id');
    }

    public function admin()
    {
        return $this->hasOne(Users::class, 'id', 'id_user');
    }
}
