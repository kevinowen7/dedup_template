<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class DataClient extends Model
{
    protected $table = 'client';
    protected $guarded = [];
}
