<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BlacklistHistory extends Model
{
    protected $table = 'history_blacklist';
    protected $guarded = [];
}
