# Pages
Berikut adalah struktur data sample:
- Dashboard -> tidak punya child
- MasterData -> punya child, jadi ada `MasterData.vue` & folder `MasterData`, isinya children dari MasterData tsb, terdiri dari:
  - Cara Pembayaran -> tidak punya child lagi, jadi langsung menggunakan `<r-page>`
  - Limit -> punya children, jadi ada Limit.vue & folder Limit
