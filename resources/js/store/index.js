import Vue from 'vue'
import Vuex from 'vuex'

import limit from './modules/limit'
import sample from './modules/sample'
import masterData from './modules/master-data'

Vue.use(Vuex)
const store = new Vuex.Store({
  modules: {
    sample,
    masterData,
    limit,
  }
})

export default store
