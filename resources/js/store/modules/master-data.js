const state = () => ({
  sample: 'This is Sample'
})

const actions = {
  GET_BOOKS: ({commit}, payload) => {
    // for request
  }
}

const mutations = {
  getData (state, data) {
    state.books = data
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}