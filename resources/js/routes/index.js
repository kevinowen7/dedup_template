import DashboardPage from '../pages/Dashboard.vue'

export default [
  {
    path:'/',
    component: () => import('../pages/Dashboard.vue'),
    name: 'dashboard'
  },
  {
    path:'/dashboard',
    component: () => import('../pages/Dashboard.vue'),
    name: 'dashboard'
  },
  {
    path:'/master-data',
    component: () => import('../pages/MasterData.vue'),
    name: 'master-data'
  },
  {
    path:'/blacklist-data',
    component: () => import('../pages/BlacklistData.vue'),
    name: 'blacklist-data',
    children: [
      {
        path: 'blacklist',
        component: () => import('../pages/BlacklistData/Blacklist.vue'),
      },
      {
        path: 'add-new',
        component: () => import('../pages/BlacklistData/AddNew.vue'),
      },
      {
        name: 'update-data',
        path: 'update/:id',
        component: () => import('../pages/BlacklistData/UpdateData.vue'),
      },
      {
        path: 'bulk-upload',
        component: () => import('../pages/BlacklistData/BulkUpload.vue'),
      },
      {
        name: 'bulk-result',
        path: 'bulk-result/:id',
        component: () => import('../pages/BlacklistData/BulkResult.vue'),
      },
    ]
  },
  {
    path:'/deduplication',
    component: () => import('../pages/Deduplication.vue'),
    name: 'deduplication',
    children: [
      {
        path: 'bulk-upload',
        component: () => import('../pages/Deduplication/BulkUpload.vue'),
      },
      {
        name: 'bulk-result',
        path: 'bulk-result/:id',
        component: () => import('../pages/Deduplication/BulkResult.vue'),
      },
      {
        path: 'single-searching',
        component: () => import('../pages/Deduplication/SingleSearching.vue'),
      },
      {
        path: 'check-history',
        component: () => import('../pages/Deduplication/CheckHistory.vue'),
      },
      {
        path: 'report-history',
        component: () => import('../pages/Deduplication/ReportHistory.vue'),
      }
    ]
  },
  {
    path:'/setting',
    component: () => import('../pages/Setting.vue'),
    name: 'setting',
    children: [
      {
        path: 'user-setting',
        component: () => import('../pages/Setting/UserSetting.vue'),
      },
      {
        path: 'create-branch',
        component: () => import('../pages/Setting/CreateBranch.vue'),
      },
      {
        path: 'add-new',
        component: () => import('../pages/Setting/AddNew.vue'),
      },
      {
        path: 'new-branch',
        component: () => import('../pages/Setting/NewBranch.vue'),
      }
    ]
  }
]