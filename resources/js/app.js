import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';

import FrontendKit from '@roketin-library/frontend-kit'
import './bootstrap';

import routes from './routes/index'
import store from './store'

Vue.use(FrontendKit, {
  // platform: 'html',
});

const router = new VueRouter({
  mode: 'history',
  routes
});

Vue.use(VueRouter)


const app = new Vue({
  components: { App },
  el: '#app',
  router,
  store
})
