<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CookieController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\BlacklistController;
use App\Http\Controllers\CheckingController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//users
Route::name('users.')->prefix('users')->group(function () {
    Route::get('/', [CookieController::class, 'getUser'])->name("users.get");
    Route::post('/store', [CookieController::class, 'store'])->name("users.store");
    Route::get('/login-checker', [CookieController::class, 'loginChecker'])->name("users.login_checker");
    Route::post('/login', [CookieController::class, 'setCookie'])->name("users.set_cookie");
    Route::post('/destroy', [CookieController::class, 'destroy'] )->name("users.destroy");
    Route::get('/logout', [CookieController::class, 'logOut'] )->name("users.log_out");
    Route::get('/cookie', [BlacklistController::class, 'getBulk'])->name("users.get_cookie");
});

Route::name('branch.')->prefix('branch')->group(function () {
    Route::get('/', [BranchController::class, 'index'])->name("branch.get");
});

Route::name('role.')->prefix('role')->group(function () {
    Route::get('/', [RoleController::class, 'index'])->name("role.get");
});

//blacklist
Route::name('blacklist.')->prefix('blacklist')->group(function () {
    Route::get('/', [BlacklistController::class, 'get'])->name("blacklist.get");
    Route::get('/get', [BlacklistController::class, 'getById'])->name("blacklist.get_by_id");
    Route::get('/get_by_bulk', [BlacklistController::class, 'getByBulkId'])->name("blacklist.get_by_bulk");
    Route::post('/store', [BlacklistController::class, 'store'])->name("blacklist.store");
    Route::post('/update', [BlacklistController::class, 'update'])->name("blacklist.update");
    Route::post('/destroy', [BlacklistController::class, 'destroy'] )->name("blacklist.destroy");

    Route::post('/update_alias', [BlacklistController::class, 'update_alias'])->name("blacklist.update_alias");
    Route::post('/destroy_alias', [BlacklistController::class, 'destroy_alias'] )->name("blacklist.destroy_alias");

    Route::get('/get_bulk', [BlacklistController::class, 'getBulk'])->name("blacklist.get_bulk");
    Route::post('/import', [BlacklistController::class, 'bulkImport'] )->name("blacklist.import");
});

//checking
Route::name('checking.')->prefix('checking')->group(function () {
    Route::get('/', [CheckingController::class, 'get'])->name("checking.get");
    Route::get('/get', [CheckingController::class, 'getById'])->name("checking.get_by_id");
    Route::get('/get_by_bulk', [CheckingController::class, 'getByBulkId'])->name("checking.get_by_bulk");
    Route::post('/store', [CheckingController::class, 'store'])->name("checking.store");
    Route::post('/report', [CheckingController::class, 'report'])->name("checking.report");

    Route::get('/get_bulk', [CheckingController::class, 'getBulk'])->name("checking.get_bulk");
    Route::post('/import', [CheckingController::class, 'bulkImport'] )->name("checking.import");
    Route::get('/get_client', [CheckingController::class, 'getDataClient'] )->name("checking.get_client");
    Route::get('/get_download', [CheckingController::class, 'getDownload'] )->name("checking.get_download");
    Route::get('/export_history', [CheckingController::class, 'exportHistory'] )->name("checking.export_history");
});

Route::name('report.')->prefix('report')->group(function () {
    Route::get('/', [ReportController::class, 'get'])->name("report.get");
    Route::get('/get', [ReportController::class, 'getById'])->name("report.get_by_id");
    Route::post('/approve_report', [ReportController::class, 'approve_report'])->name("report.approve_report");
    Route::post('/reject_report', [ReportController::class, 'reject_report'])->name("report.reject_report");
    Route::get('/download_pdf', [ReportController::class, 'downloadPdf'] )->name("checking.download_pdf");
    Route::get('/download_img', [ReportController::class, 'downloadImg'] )->name("checking.download_img");
});

Route::name('dashboard.')->prefix('dashboard')->group(function () {
    Route::get('/', [DashboardController::class, 'get'])->name("dashboard.get");
});
