<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryDataClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_data_client', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_history');
            $table->string('name')->nullable();
            $table->dateTime('DateOfBirth')->nullable();
            $table->dateTime('OpenedDate')->nullable();
            $table->string('KTP')->nullable();
            $table->string('ClientID')->nullable();
            $table->string('RegionID')->nullable();
            $table->string('CenterID')->nullable();
            $table->string('CenterName')->nullable();
            $table->string('accountid')->nullable();
            $table->string('OurBranchID')->nullable();
            $table->string('status')->nullable();
            $table->string('wo_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_data_client');
    }
}
