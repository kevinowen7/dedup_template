<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryBlacklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_blacklist', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_history');
            $table->string('name')->nullable();
            $table->string('nik')->nullable();
            $table->string('birth_date')->nullable();
            $table->longText('data_mentah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_blacklist');
    }
}
