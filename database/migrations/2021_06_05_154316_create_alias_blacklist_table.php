<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAliasBlacklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alias_blacklist', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_blacklist');
            $table->string('alias_name')->nullable();
            $table->longText('address')->nullable();
            $table->string('nik')->nullable();
            $table->string('birth_date')->nullable();
            $table->longText('data_mentah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alias_blacklist');
    }
}
