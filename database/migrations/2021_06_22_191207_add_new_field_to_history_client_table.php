<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldToHistoryClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_data_client', function (Blueprint $table) {
            $table->string('FirstDisbursementDate')->nullable();
            $table->string('GroupID')->nullable();
            $table->string('GroupName')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_data_client', function (Blueprint $table) {
            $table->dropColumn('FirstDisbursementDate');
            $table->dropColumn('GroupID');
            $table->dropColumn('GroupName');
        });
    }
}
